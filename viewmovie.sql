/* Delete the tables if they already exist */
drop table if exists Movie;
drop table if exists Reviewer;
drop table if exists Rating;

/* Create the schema for our tables */
create table Movie(mID int, title text, year int, director text);
create table Reviewer(rID int, name text);
create table Rating(rID int, mID int, stars int, ratingDate date);

/* Populate the tables with our data */
insert into Movie values(101, 'Gone with the Wind', 1939, 'Victor Fleming');
insert into Movie values(102, 'Star Wars', 1977, 'George Lucas');
insert into Movie values(103, 'The Sound of Music', 1965, 'Robert Wise');
insert into Movie values(104, 'E.T.', 1982, 'Steven Spielberg');
insert into Movie values(105, 'Titanic', 1997, 'James Cameron');
insert into Movie values(106, 'Snow White', 1937, null);
insert into Movie values(107, 'Avatar', 2009, 'James Cameron');
insert into Movie values(108, 'Raiders of the Lost Ark', 1981, 'Steven Spielberg');

insert into Reviewer values(201, 'Sarah Martinez');
insert into Reviewer values(202, 'Daniel Lewis');
insert into Reviewer values(203, 'Brittany Harris');
insert into Reviewer values(204, 'Mike Anderson');
insert into Reviewer values(205, 'Chris Jackson');
insert into Reviewer values(206, 'Elizabeth Thomas');
insert into Reviewer values(207, 'James Cameron');
insert into Reviewer values(208, 'Ashley White');

insert into Rating values(201, 101, 2, '2011-01-22');
insert into Rating values(201, 101, 4, '2011-01-27');
insert into Rating values(202, 106, 4, null);
insert into Rating values(203, 103, 2, '2011-01-20');
insert into Rating values(203, 108, 4, '2011-01-12');
insert into Rating values(203, 108, 2, '2011-01-30');
insert into Rating values(204, 101, 3, '2011-01-09');
insert into Rating values(205, 103, 3, '2011-01-27');
insert into Rating values(205, 104, 2, '2011-01-22');
insert into Rating values(205, 108, 4, null);
insert into Rating values(206, 107, 3, '2011-01-15');
insert into Rating values(206, 106, 5, '2011-01-19');
insert into Rating values(207, 107, 5, '2011-01-20');
insert into Rating values(208, 104, 3, '2011-01-02');

/* Create the views */
create view LateRating as
  select distinct R.mID, title, stars, ratingDate
  from Rating R, Movie M
  where R.mID = M.mID
  and ratingDate > '2011-01-20';

create view HighlyRated as
  select mID, title
  from Movie
  where mID in (select mID from Rating where stars > 3);

create view NoRating as
  select mID, title
  from Movie
  where mID not in (select mID from Rating);


select * from Movie;

select * from NoRating;

select * from LateRating;

create trigger LateRatingMovieTitle
instead of update of title on LateRating
for each row
when NEW.mID = OLD.mID
begin
	update Movie set title = NEW.title where mID = NEW.mID;
end;

update LateRating set title='STAR WARS' where mID = 101;

drop trigger LateRatingStarsUpdate;
create trigger LateRatingStarsUpdate
instead of update of stars on LateRating
for each row
when NEW.mID = OLD.mID and NEW.ratingDate = OLD.ratingDate
begin
	update Rating set stars = NEW.stars where mID = NEW.mID and ratingDate = NEW.ratingDate;
end;

select * from LateRating;

update LateRating set stars = 20 where mID = 101;

drop trigger LateRatingIDUpdate;
create trigger LateRatingIDUpdate
instead of update of mID on LateRating
for each row
begin
	update Movie set mID = NEW.mID where mID = OLD.mID;
	update Rating set mID = NEW.mID where mID = OLD.mID;
end;

drop trigger LateRatingUpdate;
create trigger LateRatingUpdate
instead of update on LateRating
for each row
when NEW.ratingDate = OLD.ratingDate
begin
	update Movie set mID = NEW.mID where mID = OLD.mID;
	update Rating set mID = NEW.mID where mID = OLD.mID;
	update Movie set title = NEW.title where mID = NEW.mID;
	update Rating set stars = NEW.stars where mID = NEW.mID and ratingDate = NEW.ratingDate;
end;

drop trigger DeleteHighlyRated;
create trigger DeleteHighlyRated
instead of delete on HighlyRated
for each row
begin
	delete from Rating where mID = OLD.mID and stars > 3;
end;

drop trigger DeleteHighlyRated;
create trigger DeleteHighlyRated
instead of delete on HighlyRated
for each row
begin
	 update Rating set stars = 3 where mID = OLD.mID and stars > 3;
end;

create trigger InsertHighlyRated
instead of insert on HighlyRated
for each row
when exists (select * from Movie where mID=NEW.mID and title=NEW.title)
begin
	insert into Rating values (201, NEW.mID, 5, NULL);
end;

create trigger NoRatingInsert
instead of insert on NoRating
for each row
begin
	delete from Rating where mID = NEW.mID;
end;

create trigger NoRatingDelete
instead of delete on NoRating
for each row
begin
	delete from Movie where mID = OLD.mID;
end;

drop trigger NoRatingDelete;
create trigger NoRatingDelete
instead of delete on NoRating
for each row
begin
	insert into Rating values (201, OLD.mID, 1, NULL);
end;