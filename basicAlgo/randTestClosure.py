# attempting to come up with some random testing code for closure

import closure
import random
import sys

RECLIM = sys.getrecursionlimit()

def unique(l):
    ret_val = []
    for item in l:
        if item not in ret_val:
            ret_val.append(item)
    return ret_val

def createPath(item, start, end, p_done, p_select, depth = 1):
    FDcreated = []

    if random.random() < p_done or depth >= RECLIM - 10:
        begin_elt = random.choice(start)
        dom = [begin_elt]  # to ensure the domain is not empty

        for dom_item in start:
            if random.random() < p_select:
                dom.append(dom_item)
        FDcreated.append((unique(dom),item))
    else:
        begin_elt = random.choice(end)
        dom = [begin_elt]  # to ensure the domain is not empty
        FDcreated.extend(createPath(begin_elt, start, end, p_done, p_select, depth+1))

        for dom_item in end:
            if random.random() < p_select:
                path = createPath(dom_item, start, end, p_done, p_select, depth+1)
                FDcreated.extend(path)
                dom.append(dom_item)
        FDcreated.append((unique(dom),item))

    return FDcreated

def buildInstance(start, end, p_done, p_select):
    start = unique(start)
    end.extend(start)
    end = unique(end)
    FD = []
    for item in end:
        FD.extend(createPath(item, start, end, p_done, p_select))
    return FD

if __name__ == "__main__":
    p_done = 0.8
    p_select = 0.4   # these two values experimentally decided to give rise to decent size problems
    for i in range(0,10000):
        start = [1,2,3,4]
        end = [5,6,7,8,9]
        FD = buildInstance(start,end,p_done,p_select)
        print "Testing round:", i
        print "Number of FD:", len(FD)
        computed_end = closure.closure(start, FD)
        for item in start:
            if not item in computed_end:
                print "start not in end"
        for item in end:
            if not item in computed_end:
                print "end not in end"
