FD = [(('A','B'),('C')), (('D'),('B')), (('A'),('D'))]

class DoesNotApply(Exception):
    def __init__(self, val = None):
        if val:
            self.value = val
        else:
            self.value = "No value given."

    def __str__(self):
        return "DoesNotApply: " + str(self.value)

def applyFD(attr, rel):
    for x in rel[0]:
        if x not in attr:
            raise DoesNotApply
    if rel[1] in attr:
        raise DoesNotApply
    attr.append(rel[1])
    return attr

def closure(attr, FD):
    cont = True
    while cont:
        cont = False
        for rel in FD:
            try:
                attr = applyFD(attr,rel)
                cont = True
            except DoesNotApply:
                pass
    return attr

if __name__ == "__main__":
    print closure(['A'], FD)
