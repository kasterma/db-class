import doctest

rels = [('A','D'),('A','C'),('B','C','D')]
FD = [(('A'),('B')),(('B'),('C')),(('C','D'),('A'))]

def attrList(rels):
    """ compute the sorted list of attributes for the relations.

    >>> attrList([(1,4),(1,3),(2,3,4)])
    [1, 2, 3, 4]

    >>> attrList([['a','b'],['e','a']])
    ['a', 'b', 'e']
    """
    attr = []
    for att in rels:
        for it in att:
            if it not in attr:
                attr.append(it)
    attr.sort()
    return attr

def setDistance(row):
    try:
        del(row['distance'])
    except KeyError:
        pass  # if distance has not been set yet, we'll set it below
    row['distance'] = sum([1 for val in row.values() if val != 0])

def setup(rels):
    """ create the table to chase through.

    >>> setup([('A','D'),('A','C'),('B','C','D')])
    [{'A': 0, 'distance': 2, 'C': 2, 'B': 1, 'D': 0}, {'A': 0, 'distance': 2, 'C': 0, 'B': 3, 'D': 4}, {'A': 5, 'distance': 1, 'C': 0, 'B': 0, 'D': 0}]
    """
    attr = attrList(rels)
    newNo = 1
    table = []

    for rel in rels:
        row = {}
        for at in rel:
            row[at] = 0
        for at in attr:
            if at not in rel:
                row[at] = newNo
                newNo += 1
        setDistance(row)
        table.append(row)

    return table

def applyFDtoRow(FDrel, row1, row2):
    """ applies a functional relation to the two rows

    updates the rows and returns True if FDrel applied and made a change

    >>> applyFDtoRow([('A'),('B')], {'A':0, 'B':1}, {'A':0, 'B':2})
    True

    >>> applyFDtoRow([('A'),('B')], {'A':0, 'B':1}, {'A':3, 'B':2})
    False
    """
    dom = FDrel [0]
    out = FDrel [1]

    for att in dom:
        if row1[att] != row2[att]:
            return False

    if row1[out] < row2[out]:
        row2[out] = row1[out]
        setDistance(row2)
        return True
    elif row2[out] < row1[out]:
        row1[out] = row2[out]
        setDistance(row1)
        return True
    else:
        return False

def chase(rels, FDs):
    """ perform the chase

    >>> chase([('A','D'),('A','C'),('B','C','D')], [('A','B'),('B','C')])
    False
    >>> chase([('A','D'),('A','C'),('B','C','D')], [(('A'),('B')),(('B'),('C')),(('C','D'),('A'))])
    True
    """

    table = setup(rels)

    cont = True  # flag for if we are done, becomes false when done

    while cont:
        cont = False
        for FDrel in FDs:
            for i in range(0,len(table)):
                for j in range(i+1,len(table)):
                    cont = applyFDtoRow(FDrel, table[i], table[j]) or cont
                    if table[i]['distance'] == 0 or table[j]['distance'] == 0:
                        return True
    return False

if __name__ == "__main__":
    doctest.testmod()
    print "tested"
