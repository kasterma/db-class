/* (setq sql-sqlite-program "sqlite3") */
/* Delete the tables if they already exist */
drop table if exists Highschooler;
drop table if exists Friend;
drop table if exists Likes;

/* Create the schema for our tables */
create table Highschooler(ID int, name text, grade int);
create table Friend(ID1 int, ID2 int);
create table Likes(ID1 int, ID2 int);

/* Populate the tables with our data */
insert into Highschooler values (1510, 'Jordan', 9);
insert into Highschooler values (1689, 'Gabriel', 9);
insert into Highschooler values (1381, 'Tiffany', 9);
insert into Highschooler values (1709, 'Cassandra', 9);
insert into Highschooler values (1101, 'Haley', 10);
insert into Highschooler values (1782, 'Andrew', 10);
insert into Highschooler values (1468, 'Kris', 10);
insert into Highschooler values (1641, 'Brittany', 10);
insert into Highschooler values (1247, 'Alexis', 11);
insert into Highschooler values (1316, 'Austin', 11);
insert into Highschooler values (1911, 'Gabriel', 11);
insert into Highschooler values (1501, 'Jessica', 11);
insert into Highschooler values (1304, 'Jordan', 12);
insert into Highschooler values (1025, 'John', 12);
insert into Highschooler values (1934, 'Kyle', 12);
insert into Highschooler values (1661, 'Logan', 12);

insert into Friend values (1510, 1381);
insert into Friend values (1510, 1689);
insert into Friend values (1689, 1709);
insert into Friend values (1381, 1247);
insert into Friend values (1709, 1247);
insert into Friend values (1689, 1782);
insert into Friend values (1782, 1468);
insert into Friend values (1782, 1316);
insert into Friend values (1782, 1304);
insert into Friend values (1468, 1101);
insert into Friend values (1468, 1641);
insert into Friend values (1101, 1641);
insert into Friend values (1247, 1911);
insert into Friend values (1247, 1501);
insert into Friend values (1911, 1501);
insert into Friend values (1501, 1934);
insert into Friend values (1316, 1934);
insert into Friend values (1934, 1304);
insert into Friend values (1304, 1661);
insert into Friend values (1661, 1025);
insert into Friend select ID2, ID1 from Friend;

insert into Likes values(1689, 1709);
insert into Likes values(1709, 1689);
insert into Likes values(1782, 1709);
insert into Likes values(1911, 1247);
insert into Likes values(1247, 1468);
insert into Likes values(1641, 1468);
insert into Likes values(1316, 1304);
insert into Likes values(1501, 1934);
insert into Likes values(1934, 1501);
insert into Likes values(1025, 1101);

create trigger FriendlyLikesAll
after insert on Highschooler
when New.name = 'Friendly'
begin
  insert into Likes 
    select New.id, id from Highschooler where grade = New.grade and not id = New.id;
end;

drop trigger FriendlyLikesAll;

select 666, id from Highschooler where grade = 11;

insert into Highschooler values (666, 'Friendly', 11);

select * from Likes where ID1=666;

select * from Likes;

delete from Likes where ID1=666;
delete from Highschooler where id=666;

create trigger CheckGrade
after insert on Highschooler
begin
  CASE 
    WHEN New.grade > 12 THEN 
       update Highschooler set grade=NULL where id = New.id;
    WHEN New.grade IS NULL THEN
       update Highschooler set grade=9 where id = New.id;
end;

drop trigger CheckGrade;
create trigger CheckGrade
after insert on Highschooler
begin
update Highschooler set grade = (select case 
     when (New.grade < 9 or New.grade > 12)
     then
       NULL
     when (New.grade IS NULL)
     then
       9
     else
       New.grade
   end)
where id = New.id;
end;
delete from Highschooler where id = 666;
insert into Highschooler values (666, 'Yes him', 9);
select * from Highschooler;

       update Highschooler set grade=12 where id = 1934;
select * from Highschooler;

update Highschooler set grade = (select case 
     when (12=12)
     then
       59
     else
       66
   end)
where id = 1934;
select * from Highschooler;

select * from Friend;



select * from Friend F where not exists (select * from Friend SF where SF.ID1 = F.ID2 and SF.ID2 = F.ID1);

insert into Friend values (777,666);


drop trigger SymmetricFriend;
create trigger SymmetricFriend
after delete on Friend
begin
	delete from Friend where ID1=Old.ID2 and ID2=Old.ID1
end;
drop trigger SymmetricFriend2;
create trigger SymmetricFriend2
after insert on Friend
begin
	insert into Friend values (New.ID2, New.ID1);
end;
insert into Friend values (666,777);
select count(*) from Friend;
delete from Friend where ID1=666 or ID2=666;
select count(*) from Friend;

drop trigger Graduate;
create trigger Graduate
after update on Highschooler
when New.grade > 12
begin
	delete from Highschooler where id = New.id;
end;
select * from Highschooler;
update Highschooler set grade=13 where id = 1101;
select * from Highschooler;

drop trigger Graduate;
create trigger Graduate
after update on Highschooler
when New.grade > 12
begin
	delete from Highschooler where id = New.id;
end;
drop trigger FriendsMove;
create trigger FriendsMove
after update on Highschooler
when New.grade = Old.grade+1
begin
	update Highschooler set grade = grade+1 where id in (select ID1 from Friend where ID2 = New.id);
end;
select * from Highschooler;
update Highschooler set grade=10 where id = 1381;
select * from Highschooler;

select * from Highschooler;

select ID1 from Friend where ID2 = 1381;

select * from Friend;

drop trigger Skism;
create trigger Skism
after update on Likes
when New.ID1=Old.ID1 and not New.ID2 = Old.ID2
begin
	delete from Friends where (ID1=Old.ID2 and ID2=New.ID2) or 
	(ID1=New.ID2 and ID2=Old.ID2);
end;

select * from Likes;
update Likes set ID2=1468 where ID1=1689 and ID2=1709;
select * from Likes;
update Likes set ID2=1709 where ID1=1689 and ID2=1468;
select * from Likes;

select * from Friend;