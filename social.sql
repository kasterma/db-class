/* Delete the tables if they already exist */
drop table if exists Highschooler;
drop table if exists Friend;
drop table if exists Likes;

/* Create the schema for our tables */
create table Highschooler(ID int, name text, grade int);
create table Friend(ID1 int, ID2 int);
create table Likes(ID1 int, ID2 int);

/* Populate the tables with our data */
insert into Highschooler values (1510, 'Jordan', 9);
insert into Highschooler values (1689, 'Gabriel', 9);
insert into Highschooler values (1381, 'Tiffany', 9);
insert into Highschooler values (1709, 'Cassandra', 9);
insert into Highschooler values (1101, 'Haley', 10);
insert into Highschooler values (1782, 'Andrew', 10);
insert into Highschooler values (1468, 'Kris', 10);
insert into Highschooler values (1641, 'Brittany', 10);
insert into Highschooler values (1247, 'Alexis', 11);
insert into Highschooler values (1316, 'Austin', 11);
insert into Highschooler values (1911, 'Gabriel', 11);
insert into Highschooler values (1501, 'Jessica', 11);
insert into Highschooler values (1304, 'Jordan', 12);
insert into Highschooler values (1025, 'John', 12);
insert into Highschooler values (1934, 'Kyle', 12);
insert into Highschooler values (1661, 'Logan', 12);

insert into Friend values (1510, 1381);
insert into Friend values (1510, 1689);
insert into Friend values (1689, 1709);
insert into Friend values (1381, 1247);
insert into Friend values (1709, 1247);
insert into Friend values (1689, 1782);
insert into Friend values (1782, 1468);
insert into Friend values (1782, 1316);
insert into Friend values (1782, 1304);
insert into Friend values (1468, 1101);
insert into Friend values (1468, 1641);
insert into Friend values (1101, 1641);
insert into Friend values (1247, 1911);
insert into Friend values (1247, 1501);
insert into Friend values (1911, 1501);
insert into Friend values (1501, 1934);
insert into Friend values (1316, 1934);
insert into Friend values (1934, 1304);
insert into Friend values (1304, 1661);
insert into Friend values (1661, 1025);
insert into Friend select ID2, ID1 from Friend;

insert into Likes values(1689, 1709);
insert into Likes values(1709, 1689);
insert into Likes values(1782, 1709);
insert into Likes values(1911, 1247);
insert into Likes values(1247, 1468);
insert into Likes values(1641, 1468);
insert into Likes values(1316, 1304);
insert into Likes values(1501, 1934);
insert into Likes values(1934, 1501);
insert into Likes values(1025, 1101);

/* find the names of all students who are friends with someone names
Gabriel */

SELECT ID
FROM Highschooler
WHERE name = 'Gabriel';

SELECT ID2
FROM Friend
WHERE ID1 IN
 (SELECT ID
  FROM Highschooler
  WHERE name = 'Gabriel');

SELECT name
FROM Highschooler
WHERE ID IN
   (SELECT ID2
    FROM Friend
    WHERE ID1 IN
     (SELECT ID
      FROM Highschooler
      WHERE name = 'Gabriel'));

/* For every student who likes someone 2 or more grades younger than
themselves, return that student's name and grade, and the name and
grade of the student they like. */

SELECT *
FROM Highschooler JOIN Friend ON ID = ID1;

SELECT H1.name, H1.grade, H2.name, H2.grade
FROM Highschooler H1 JOIN Highschooler H2 JOIN Likes 
    ON H1.ID = ID1 AND H2.ID=ID2
WHERE H1.grade - H2.grade >= 2;

/* For every pair of students who both like each other, return the
name and grade of both students. Include each pair only once, with the
two names in alphabetical order. */

FROM Highschooler H1 JOIN Highschooler H2 JOIN Likes L1
    JOIN Highschooler H3 JOIN Highschooler H4 JOIN Likes L2
    ON H1.ID = L1.ID1 AND H2.ID=L2.ID2 AND H3.ID = L2.ID1
    AND H4.ID = L2

SELECT L1.ID1, L1.ID2
FROM Likes L1 JOIN Likes L2 ON
  L1.ID1 = L2.ID2 AND L1.ID2 = L2.ID1;

SELECT H1.name,H1.grade, H2.name, H2.grade
FROM
((SELECT L1.ID1 id1, L1.ID2 id2
  FROM Likes L1 JOIN Likes L2 ON
    L1.ID1 = L2.ID2 AND L1.ID2 = L2.ID1) AS syml JOIN
  Highschooler H1 JOIN Highschooler H2 ON
  H1.ID = syml.id1 AND H2.ID= syml.id2)
WHERE H1.name < H2.name;

/* Find all students who do not appear in the Likes table (as a
student who likes or is liked) and return their names and grades. Sort
by grade, then by name within each grade. */

(SELECT ID1 as id FROM Likes) UNION (SELECT ID2 as id FROM Likes);

SELECT H.name, H.grade
FROM Highschooler H
WHERE H.ID not in
(SELECT ID1 as id FROM Likes UNION SELECT ID2 as id FROM Likes);

/* For every situation where student A likes student B, but we have no
information about whom B likes (that is, B does not appear as an ID1
in the Likes table), return A and B's names and grades. */

SELECT Hids.ID id
FROM
(SELECT H.ID as id FROM Highschooler H) Hids
WHERE Hids.ID not in 
(SELECT L1.ID1 as id FROM Likes L1 UNION SELECT L2.ID2 as id FROM Likes L2);

SELECT *
FROM
(SELECT Hids.ID id
 FROM
 (SELECT H.ID as id FROM Highschooler H) Hids
 WHERE Hids.ID not in 
 (SELECT L1.ID1 as id FROM Likes L1)
) noinfo NATURAL JOIN Highschooler;


 SELECT *
 FROM Likes
 WHERE ID2 in
 (SELECT Hids.ID id
  FROM
  (SELECT H.ID as id FROM Highschooler H) Hids
  WHERE Hids.ID not in 
  (SELECT L1.ID1 as id FROM Likes L1));

SELECT h1.name, h1.grade, h2.name, h2.grade
FROM
(SELECT *
 FROM Likes
 WHERE ID2 in
 (SELECT Hids.ID id
  FROM
  (SELECT H.ID as id FROM Highschooler H) Hids
  WHERE Hids.ID not in 
  (SELECT L1.ID1 as id FROM Likes L1))) lnoi JOIN Highschooler h1
JOIN Highschooler h2 ON ID1 = h1.ID AND ID2 = h2.ID;

/* Find names and grades of students who only have friends in the same
grade. Return the result sorted by grade, then by name within each
grade.  */

SELECT name, grade
FROM Highschooler
WHERE ID not in
(SELECT H1.ID id
FROM
Highschooler H1 JOIN Friend JOIN Highschooler H2
ON H1.ID = Friend.ID1 AND H2.ID = Friend.ID2
WHERE not H1.grade = H2.grade)
ORDER BY grade, name;

/* For each student A who likes a student B where the two are not
friends, find if they have a friend C in common (who can introduce
them!). For all such trios, return the name and grade of A, B, and
C. */

SELECT *
FROM Likes L
WHERE NOT EXISTS (SELECT * FROM Friend F WHERE F.ID1 = L.ID1 AND F.ID2 = L.ID2);

SELECT H1.name, H1.grade, H2.name, H2.grade
FROM
Highschooler AS H1
 JOIN
  (SELECT *
   FROM Likes L
   WHERE NOT EXISTS 
   (SELECT * FROM Friend F WHERE F.ID1 = L.ID1 AND F.ID2 = L.ID2)) AS lnof
 JOIN 
 Highschooler AS H2 
ON H1.ID = lnof.ID1 AND H2.ID = lnof.ID2;

SELECT F1.ID1 A, F2.ID2 B, F1.ID2 C
FROM
( Friend AS F1
  JOIN
  (SELECT *
   FROM Likes L
   WHERE NOT EXISTS 
     (SELECT * FROM Friend F WHERE F.ID1 = L.ID1 AND F.ID2 = L.ID2))
  AS lnof
  JOIN
  Friend AS F2
ON F1.ID1 = lnof.ID1 AND F2.ID2 = lnof.ID2 AND F1.ID2 = F2.ID1);

SELECT *
FROM
(Highschooler HA
 JOIN
 Highschooler HB
 JOIN
 Highschooler HC
 JOIN
(SELECT F1.ID1 idA, F2.ID2 idB, F1.ID2 idC
 FROM
 ( Friend AS F1
   JOIN
   (SELECT *
    FROM Likes L
    WHERE NOT EXISTS 
      (SELECT * FROM Friend F WHERE F.ID1 = L.ID1 AND F.ID2 = L.ID2))
   AS lnof
   JOIN
   Friend AS F2
 ON F1.ID1 = lnof.ID1 AND F2.ID2 = lnof.ID2 AND F1.ID2 = F2.ID1)) AS idABC
ON  HA.ID = idABC.idA
AND HB.ID = idABC.idB
AND HC.ID = idABC.idC;

SELECT A.name, A.grade, B.name, B.grade, C.name, C.grade
FROM
Highschooler A 
JOIN Friend F1 
JOIN Highschooler C 
JOIN Friend F2 
JOIN Highschooler B
ON A.ID = F1.ID1 AND C.ID = F1.ID2
AND F2.ID2 = B.ID AND F1.ID2 = F2.ID1
WHERE 
NOT EXISTS (SELECT * FROM Friend FF WHERE FF.ID1 = A.ID and FF.ID2 = B.ID)
AND
EXISTS (SELECT * FROM Likes LL WHERE LL.ID1 = A.ID and LL.ID2 = B.ID);


/* Find the difference between the number of students in the school
and the number of different first names. */

SELECT count(ID) FROM Highschooler;

SELECT count(ID) - count(distinct name) from Highschooler;

/* Find the name and grade of all students who are liked by more than
one other student. */

SELECT name, grade
FROM
Highschooler
WHERE
ID IN
(SELECT L1.ID2
FROM
Likes L1
JOIN
Likes L2
ON L1.ID2 = L2.ID2
WHERE NOT L1.ID1 = L2.ID1);

/* It's time for the seniors to graduate. Remove all 12th graders from
Highschooler. */

delete from Highschooler where grade=12;

/* If two students A and B are friends, and A likes B but not
vice-versa, remove the Likes tuple. */

select *
from Friend AS F
where EXISTS (select * from Likes L where L.ID1 = F.ID1 AND L.ID2 = F.ID2)
AND
NOT EXISTS (select * from Likes L where L.ID2 = F.ID1 AND L.ID1 = F.ID2);

/* The following works in sqlight, though it doesn't in mysql */
delete
from Likes
where
NOT EXISTS (select * from Likes L2 where Likes.ID1 = L2.ID2 AND Likes.ID2 = L2.ID1)
AND
EXISTS (select * from Friend AS F where Likes.ID1 = F.ID1 AND Likes.ID2 = F.ID2);